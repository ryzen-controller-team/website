# Ryzen Controller Website

This is the source for the Ryzen Controller website. It gives a brief description of how Ryzen Controller works and provides links to downloads, the Discord server, the Patreon page, and any relevant GitLab/GitHub projects.

## Built With

* [ZenScroll](https://zengabor.github.io/zenscroll/) - For smooth scrolling
* [Parallax.js](https://pixelcog.github.io/parallax.js/) - For the parallax background

## Authors

* **Caleb Kamrath** - *Initial Work* - [Graywire](https://graywire.net/portfolio)

## License

This project is licensed under the Creative Commons Zero 1.0 License - see the [LICENSE.md](LICENSE.md) file for details.
